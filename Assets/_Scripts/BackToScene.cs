﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackToScene : MonoBehaviour {

	public GameObject lightSource;
	private GameObject oldLight;

	void Start() {
		oldLight = GameObject.FindGameObjectWithTag ("light");
		if (oldLight != null)
			Destroy (oldLight.gameObject);
		Instantiate(lightSource, new Vector3(1000, 1000, 1000), Quaternion.identity);
		lightSource = GameObject.FindGameObjectWithTag ("light");
		lightSource.transform.Rotate (new Vector3 (50, 60, 0));
	}

	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape)) {
			SceneManager.LoadScene ("Menu");
		}
	}
}
