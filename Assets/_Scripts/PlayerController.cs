﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	private Rigidbody rb;
	private int count;

	public float speed = 1.0f;
	public Text countText;
	public Text winText;

	void Start() {
		count = 0;
		changeCountText ();
		changeWinText ("");
		rb = GetComponent<Rigidbody> ();
	}

	void FixedUpdate() {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0, moveVertical);
		rb.AddForce(movement * speed);
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag("pickup")) {
			count++;
			changeCountText ();
			other.gameObject.SetActive(false);
			if(count >= 13)
				changeWinText ("You Win!!!");
		}
	}

	void changeCountText() {
		countText.text = "Count: " + count.ToString ();
 	}

	void changeWinText(string txt) {
		winText.text = txt;
	}

}
