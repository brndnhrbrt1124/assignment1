﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MissileHandler : MonoBehaviour {

	public GameObject piece;
	public int forceRange = 1;
	public int windRange = 1;
	public int howManyPieces = 100;

	private Vector3 force;
	private Vector3 wind;
	private bool windFlow = true;

	void Start() {
		randomizeForce ();
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag("target")) {
			windFlow = true;
			for (int i = 0; i < howManyPieces; i++) {
				Vector3 pos = new Vector3 (other.transform.position.x, other.transform.position.y + 10.0f, other.transform.position.z);
				Instantiate (piece, pos, Quaternion.identity);
			}

			GameObject[] pieces = GameObject.FindGameObjectsWithTag ("piece");
			foreach (GameObject p in pieces) {
				randomizeForce ();
				p.GetComponent<Rigidbody> ().AddForce (force);
			}

			Destroy (other.gameObject);
			Destroy (transform.gameObject);
		}

		if (other.gameObject.CompareTag ("water") || other.gameObject.CompareTag ("floor")) {
			Destroy (transform.gameObject);
		}
	}

	void randomizeForce() {
		force = new Vector3 (Random.Range(0, forceRange), Random.Range(0, forceRange), Random.Range(0, forceRange));
	}
}
