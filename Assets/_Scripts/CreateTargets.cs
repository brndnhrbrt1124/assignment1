﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateTargets : MonoBehaviour {

	public GameObject target;
	public int count = 0;
	public Text CountText;
	public AudioClip explode;

	private bool firstFlag = false;

	// Use this for initialization
	void Start () {
		createNewTarget ();
		count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		GameObject targets = GameObject.FindGameObjectWithTag ("target");
		if (targets == null) {
			createNewTarget ();
		}
		changeCountText ();
	}

	void createNewTarget() {
		if (firstFlag) {
			AudioSource audsrc = transform.GetComponent<AudioSource> ();
			audsrc.PlayOneShot (explode, 0.441F);
		} else {
			firstFlag = true;
		}
		
		count++;
		Instantiate(target, new Vector3(Random.Range(-70, 70), Random.Range(0, 50), Random.Range(50, 150)), Quaternion.identity);
	}

	void changeCountText() {
		CountText.text = "Count: " + count.ToString ();
	}
}
