﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShipController : MonoBehaviour {

	public GameObject missile;
	public GameObject gun;
	public GameObject camera;
	public float incBy = 1.0f;
	public float incRotBy_y = 1.0f;
	public float incRotBy_x = 1.0f;
	public float x_lim = 3000.0f;
	public float y_lim = 3000.0f;
	public float z_lim = 3000.0f;
	public int windRange = 1000;
	public bool canShoot = true;
	public Text VectorText;
	private Text comboText;
//	public Text windText;
	public AudioClip shoot;

	private AudioSource audio;
	private Vector3 wind;
	private Vector3 offset;
	private float xForce = 0.0f;
	private float yForce = 0.0f;
	private float zForce = 2000.0f;
	private bool windFlow = true;

	void Start() {
		offset = camera.transform.position - transform.position;
		comboText = GameObject.FindGameObjectWithTag ("comboText").GetComponent<Text>();
		comboText.text = "";
//		randomizeWind ();
	}

	void LateUpdate () {
		GameObject miss = GameObject.FindGameObjectWithTag ("missile");
		if (miss != null) {
			camera.transform.position = miss.transform.position + offset * 2;
//			if (windFlow) {
////				randomizeWind ();
//				Rigidbody rb = miss.transform.GetComponent<Rigidbody> ();
//				rb.AddForce (wind);
//				windFlow = false;
//			}

		} else {
			camera.transform.position = transform.position + offset * 2;
			canShoot = true;
		}

		VectorText.text = "X: " + xForce.ToString () + " Y: " + yForce.ToString () + " Z: " + zForce.ToString ();

		if (Input.GetKeyDown (KeyCode.D) && xForce < x_lim) {
			xForce += incBy;
			gun.transform.Rotate (new Vector3 (0.0f, incRotBy_y, 0.0f));
		}
		if (Input.GetKeyDown (KeyCode.A)  && xForce > -x_lim) {
			xForce -= incBy;
			gun.transform.Rotate (new Vector3 (0.0f, -incRotBy_y, 0.0f));
		}

		if (Input.GetKeyDown (KeyCode.W) && yForce < y_lim) {
			yForce += incBy;
			gun.transform.Rotate (new Vector3 (-incRotBy_x, 0.0f, 0.0f));
		}
		if (Input.GetKeyDown (KeyCode.S) && yForce > -900) {
			yForce -= incBy;
			gun.transform.Rotate (new Vector3 (incRotBy_x, 0.0f, 0.0f));
		}

		if (Input.GetKeyDown (KeyCode.E) && zForce < y_lim) {
			zForce += incBy;
		}
		if (Input.GetKeyDown (KeyCode.Q) && zForce > -y_lim) {
			zForce -= incBy;
		}

		if (Input.GetKeyDown (KeyCode.Space) && canShoot)
			launchMissile ();
		else if (Input.GetKeyDown(KeyCode.Space) && !canShoot)
			destroyAllPreviousMissiles ();
	}

	private void destroyAllPreviousMissiles() {
		comboText.text = "";
		GameObject[] pieces = GameObject.FindGameObjectsWithTag ("piece");
		foreach (GameObject p in pieces) {
			Destroy (p);
		}
		Destroy (GameObject.FindGameObjectWithTag("missile"));
		camera.transform.position = transform.position + offset;
		canShoot = true;
		windFlow = true;
	}

	private void launchMissile() {
		comboText.text = "";
//		audio.PlayOneShot(shoot, 0.441F);
		AudioSource audsrc = transform.GetComponent<AudioSource> ();
		audsrc.PlayOneShot (shoot, 0.441F);
		canShoot = false;
		Instantiate(missile, new Vector3(0.0f, 1.5f, transform.position.z + 10.0f), Quaternion.identity);
		GameObject miss = GameObject.FindGameObjectWithTag ("missile");
		Rigidbody rb = miss.GetComponent<Rigidbody>();
		rb.AddForce(new Vector3(xForce, yForce, zForce));
	}

//	void randomizeWind() {
//		wind = new Vector3 (Random.Range(-windRange, windRange), Random.Range(0, 0), Random.Range(0, 0));
//		windText = GameObject.FindGameObjectWithTag ("windText").GetComponent<Text>();
//		windText.text = "X-Wind: " + wind.x.ToString();
//		print (windText.text);
//	}
}
