﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class pieceController : MonoBehaviour {

	public int howManyPieces = 100;
	private Vector3 force;
	public int forceRange = 1;
	private Text comboText;

	// Use this for initialization
	void Start () {
		comboText = GameObject.FindGameObjectWithTag ("comboText").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator removeText()
	{
		yield return new WaitForSeconds(3);
		comboText.text = "";
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("water") || other.gameObject.CompareTag("floor")) {
			Destroy(transform.gameObject);
		}
		if (other.gameObject.CompareTag ("target")) {
			for (int i = 0; i < howManyPieces; i++) {
				Vector3 pos = new Vector3 (other.transform.position.x, other.transform.position.y + 10.0f, other.transform.position.z);
				Instantiate (transform, pos, Quaternion.identity);
			}

			GameObject[] pieces = GameObject.FindGameObjectsWithTag ("piece");
			foreach (GameObject p in pieces) {
				randomizeForce ();
				p.GetComponent<Rigidbody> ().AddForce (force);
			}

			Destroy (other.gameObject);
			comboText.text = "COMBO!";
			removeText ();
		}
	}

	void randomizeForce() {
		force = new Vector3 (Random.Range(0, forceRange), Random.Range(0, forceRange), Random.Range(0, forceRange));
	}
}
